## Compiling Vue changes:
`npm run serve` from terminal would create and move the files to the server

OR

frontend run `mvn install `and rerun the server

OR

`npm run build`

## To push on heroku:
make sure in frontend pom there's no extra commands in build
`git push heroku`

## Killing porcesses in Windows running on same port in case application shut down unexpectedly (Intelij Ultimate 30 minute limit causes this)
`netstat -ano | findstr :<PORT>`

`taskkill /PID <PID> /F`
