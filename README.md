## GENU - Generated Menu application

I created this application with the intent to reduce the time spent trying to remember what dish to cook tomorrow or next week at the table. 

## Sample application:
Currently the application is deployed on heroku for testing and visualization and it can be found at this link https://murmuring-fortress-98362.herokuapp.com/#/
## How it works:
At the moment, GENU takes a json file with a specific format containing all the dishes you can cook (can include custom entries as well, such as Takeout). The format is the following:

`"dish":{`\
`"name":"Pork rice noodles",`\
`"carbs":["NOODLES"],`\
`"meat":["PORK"],`\
`"vegetable":["MUSHROOM, CARROT"],`\
`"price":3,`\
`"chef":["???"],`\
`"days": 2`\
`}`

It will try to generate a 7 day menu taking into consideration various properties (right now `carbs` and `price`). 

## Features:
- generates a 7-day menu
- you regenerate a single day from the current menu
- can save a menu to a file
- it loads the menu saved for the current week, if it exists, otherwise it generated a new one
- shows all the available dishes
- sorting in the All dishes page
- filtering in the All dishes page
- balancing generated menus on various criteria given as input
- reshaping the UI with custom designed theme


## Known issues:
- Generated files are currently saved in the resource folder of backend, same for the input file with dishes

## Future features:
- downloading files for certain dates
- ~~status bar for server response~~
- ~~sorting in the All dishes page~~
- ~~filtering in the All dishes page~~
- ~~balancing generated menus on various criteria given as input~~
- more responsive UI (so the tables can be seen better on phones)
- various properties given as input when generating a single dish or a whole menu
- ~~reshaping the UI with custom designed theme~~
- ability to add/remove/edit an existing dish 
- allow multiple users with their own session or shared session
- ability to swap days around
- generate dish based on previous requests of ghosting a specific dish, which should be recommended less often unless requested by input property