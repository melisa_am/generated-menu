##Small tasks (to add the features written on paper)
- document every complex method
- add logger to the other classes
- change the server to work with request/replies for all the requests + adjust frontend accordingly
- add page to view previously saved menus


##Minor refactor:
- generify json toolkit, should not depend on dish
- move default values in a different location accessible everywhere (?)

##Major refactors:
- generify repository (?)
- recheck methods if they can be refactored
- change the json file into a db
- add CRUD for dish
- change carbs/meat/vegetables/chef to multiple choice instead of free text

##Bug fixing
- saveMenu doesn't work anymore in the latest iteration - should be fixed

##Task order:
- saveMenu doesn't work anymore in the latest iteration - should be fixed
- change the server to work with request/replies for all the requests + adjust frontend accordingly
- document every complex method