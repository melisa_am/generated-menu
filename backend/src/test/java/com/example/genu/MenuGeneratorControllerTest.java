package com.example.genu;

import com.example.genu.controller.MenuGeneratorController;
import com.example.genu.domain.BalanceType;
import com.example.genu.domain.Dish;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.*;

public class MenuGeneratorControllerTest {

    public static final String ANY = "???";
    MenuGeneratorController menuGeneratorController;

    public MenuGeneratorControllerTest() {
        menuGeneratorController  = new MenuGeneratorController("../backend/src/main/resources/dishes-test.json");
    }

    @Test(description = "send to the controller method generateWeeklyMenu the default values of <7, CARBS, TRUE>")
    public void defaultGenerateWeeklyMenu() {
        int numberOfDays = 7;
        int totalPriceAllowed = 7;
        BalanceType balanceType = BalanceType.CARBS;
        List<Dish> dishes = menuGeneratorController.generateWeeklyMenu(totalPriceAllowed, balanceType, true);

        assertEquals(dishes.size(), numberOfDays);
        assertTrue(getTotalPrice(dishes) <= totalPriceAllowed);
        assertTrue(isBalanced(dishes, balanceType));
    }

    @Test(description = "send to the controller method generateWeeklyMenu the default values of <3, CARBS, TRUE>")
    public void invalidPrice() {
        int totalPriceAllowed = 3;
        BalanceType balanceType = BalanceType.CARBS;

        try {
            menuGeneratorController.generateWeeklyMenu(totalPriceAllowed, balanceType, true);
            fail();
        } catch (RuntimeException e) {
            assertTrue(true);
        }
    }

    @Test(description = "send to the controller method generateWeeklyMenu the default values of <20, MEAT, TRUE>")
    public void priceHigherThanDefault() {
        int numberOfDays = 7;
        int totalPriceAllowed = 20;
        BalanceType balanceType = BalanceType.MEAT;
        List<Dish> dishes = menuGeneratorController.generateWeeklyMenu(totalPriceAllowed, balanceType, true);

        assertEquals(dishes.size(), numberOfDays);
        assertTrue(getTotalPrice(dishes) <= totalPriceAllowed);
        assertTrue(isBalanced(dishes, balanceType));
    }

    @Test(description = "generate a menu multiple times to assure that the available dishes are not removed/changed in the local cache")
    public void multipleMenuGenerations() {
        int numberOfDays = 7;
        int totalPriceAllowed = 13;
        BalanceType balanceType = BalanceType.CARBS;

        for (int i = 0; i < 10000; i++) {
            System.out.println(i);
            List<Dish> dishes = menuGeneratorController.generateWeeklyMenu(totalPriceAllowed, balanceType, true);

            assertEquals(dishes.size(), numberOfDays);
            assertTrue(getTotalPrice(dishes) <= totalPriceAllowed);
            assertTrue(isBalanced(dishes, balanceType));
        }
    }

    private boolean isBalanced(List<Dish> dishes, BalanceType balanceType) {
        List<String> previousBalanceValue = new ArrayList<>();

        for (int i = 0, dishesSize = dishes.size(); i < dishesSize; i++) {
            Dish dish = dishes.get(i);
            if (previousBalanceValue.isEmpty()) {
                previousBalanceValue = getStringList(dish, balanceType);
            } else {
                if (!previousBalanceValue.contains(ANY) && !getStringList(dish, balanceType).contains(ANY) &&
                        previousBalanceValue.containsAll(getStringList(dish, balanceType)) &&
                        getStringList(dish, balanceType).containsAll(previousBalanceValue)
                ) {
                        if (!dish.getName().equals(dishes.get(i - 1).getName())) {
                            System.out.println(dishes);
                            return false;
                    }
                }
                previousBalanceValue = getStringList(dish, balanceType);
            }
        }

        return true;
    }

    private List<String> getStringList(Dish dish, BalanceType balanceType) {
        switch (balanceType) {
            case CARBS:
               return dish.getCarbs();
            case MEAT:
                return dish.getMeat();
            case VEGETABLE:
                return dish.getVegetable();
            case CHEF:
                return dish.getChef();
            default:
                throw new RuntimeException("Type not handled");
        }
    }

    private Integer getTotalPrice(List<Dish> dishes) {
        int totalPrice = 0;
        String previousDishName = "";
        for (Dish dish : dishes) {
            if (!dish.getName().equals(previousDishName)) {
                totalPrice = totalPrice + dish.getPrice();
            }
            previousDishName = dish.getName();
        }

        return totalPrice;
    }
}
