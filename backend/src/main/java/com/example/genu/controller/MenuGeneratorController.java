package com.example.genu.controller;

import com.example.genu.domain.BalanceType;
import com.example.genu.domain.Dish;
import com.example.genu.repository.DishRepository;
import com.example.genu.repository.FileDishRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MenuGeneratorController implements Controller {
	private final int NUMBER_OF_TRIES_UNTIL_BACKTRACKING = 3;
	private final int HIGHEST_PRICE = 5;
	private final int NUMBER_OF_DAYS_IN_WEEK = 7;
	private final int DEFAULT_TOTAL_MENU_PRICE = 7;
	private final String ANY = "???";
	private final BalanceType DEFAULT_BALANCE_TYPE = BalanceType.CARBS;
	private final boolean DEFAULT_ALLOW_TAKEOUT = false;
	private final Dish TAKEOUT = Dish.DishBuilder.newBuilder()
		.setName("Takeout")
		.setCarbs(Collections.singletonList(ANY))
		.setChef(Collections.singletonList(ANY))
		.setMeat(Collections.singletonList(ANY))
		.setVegetable(Collections.singletonList(ANY))
		.setPrice(1)
		.setDays(1)
		.build();

	private final Logger LOGGER = LogManager.getLogger(MenuGeneratorController.class);

	private final DishRepository dishRepository;
	private final List<Dish> knownDishes;
	private List<Dish> latestSavedMenu;

	public MenuGeneratorController(String fileName) {
		dishRepository = new FileDishRepository(fileName);
		knownDishes = dishRepository.getDishes();
	}

	public List<Dish> getAllDishes() {
		return new ArrayList<>(knownDishes);
	}

	public List<Dish> getLatestMenu() {
		if (latestSavedMenu == null || latestSavedMenu.isEmpty() || !dishRepository.isCurrentDateLoaded()) {
			LOGGER.info("No previous data loaded for current week.");
			List<Dish> loadedMenu = dishRepository.getMenuIfExists();
			if (!loadedMenu.isEmpty()) {
				LOGGER.info("Loaded {} dishes for current week", loadedMenu::size);
				latestSavedMenu = loadedMenu;
			} else {
				LOGGER.info("Found no data to load. Generating new menu");
				latestSavedMenu = generateWeeklyMenu(DEFAULT_TOTAL_MENU_PRICE, DEFAULT_BALANCE_TYPE, DEFAULT_ALLOW_TAKEOUT);
			}
		}

		return latestSavedMenu;
	}

	public List<Dish> generateWeeklyMenu(Integer totalPrice, BalanceType balanceType, boolean allowTakeout) {
		List<Dish> generatedMenu = new ArrayList<>();
		List<Dish> allAvailableDishes = getAllDishes();
		if (allowTakeout) {
			allAvailableDishes.add(TAKEOUT);
		}
		generateWeeklyMenu(
			generatedMenu,
			allAvailableDishes,
			totalPrice,
			balanceType,
			0
		);

		return generatedMenu;
	}

	private boolean generateWeeklyMenu(
			List<Dish> generatedMenu,
			List<Dish> availableDishes,
			Integer totalPrice,
			BalanceType balanceType,
			int tries
	) {
		if (generatedMenu.size() == NUMBER_OF_DAYS_IN_WEEK) {
			return true;
		}

		Integer generatedMenuCurrentTotalPrice = getTotalPrice(generatedMenu);
		if (totalPrice - generatedMenuCurrentTotalPrice > 0) {
			availableDishes.removeIf(dish -> dish.getPrice() > (totalPrice - generatedMenuCurrentTotalPrice) / (NUMBER_OF_DAYS_IN_WEEK - generatedMenu.size()));
		}

		if (availableDishes.size() < NUMBER_OF_DAYS_IN_WEEK - generatedMenu.size()) {
			throw new RuntimeException("Could not generate menu, not enough recipes in the database");
		}

		if (tries == NUMBER_OF_TRIES_UNTIL_BACKTRACKING || totalPrice - generatedMenuCurrentTotalPrice <= 0) {
			Dish lastAddedDish = generatedMenu.remove(generatedMenu.size() - 1);
			availableDishes.add(lastAddedDish);
			tries = 0;
		}

		Collections.shuffle(availableDishes);
		Dish dish = availableDishes.get(0);

		if (!isDishValidForGivenMenu(dish, generatedMenu, totalPrice, balanceType)) {
			return generateWeeklyMenu(
				generatedMenu,
				availableDishes,
				totalPrice,
				balanceType,
				tries + 1
			);
		}

		for (int i = 0; i < dish.getDays(); i++) {
			generatedMenu.add(dish);

			if (generatedMenu.size() == NUMBER_OF_DAYS_IN_WEEK) {
				break;
			}
		}

		availableDishes.remove(dish);
		return generateWeeklyMenu(
			generatedMenu,
			availableDishes,
			totalPrice,
			balanceType,
			tries
		);
	}

	private boolean isDishValidForGivenMenu(Dish dish, List<Dish> generatedMenu, Integer totalPriceAllowed, BalanceType balanceType) {
		if (generatedMenu.isEmpty()) {
			return true;
		}

		if (!isMenuBalanced(generatedMenu, dish, balanceType)) {
			return false;
		}

		if (getTotalPrice(generatedMenu) + dish.getPrice() > totalPriceAllowed) {
			return false;
		}

		Dish expensiveDish = generatedMenu.stream()
			.filter(generatedDish -> generatedDish.getPrice() == HIGHEST_PRICE)
			.findFirst()
			.orElse(null);
		if (expensiveDish != null && dish.getPrice() == HIGHEST_PRICE) {
			return false;
		}

		return true;
	}

	private boolean isMenuBalanced(List<Dish> generatedMenu, Dish dish, BalanceType balanceType) {
		Dish previousDish = generatedMenu.get(generatedMenu.size() - 1);
		switch (balanceType) {
			case CARBS:
				if (!dish.getCarbs().contains(ANY) && !previousDish.getCarbs().contains(ANY) && previousDish.getCarbs().equals(dish.getCarbs())) {
					return false;
				}
				break;
			case MEAT:
				if (!dish.getMeat().contains(ANY) && !previousDish.getMeat().contains(ANY) && previousDish.getMeat().equals(dish.getMeat())) {
					return false;
				}
				break;
			case VEGETABLE:
				if (!dish.getVegetable().contains(ANY) && !previousDish.getVegetable().contains(ANY) && previousDish.getVegetable().equals(dish.getVegetable())) {
					return false;
				}
				break;
			case CHEF:
				if (!dish.getChef().contains(ANY) && !previousDish.getChef().contains(ANY) && previousDish.getChef().equals(dish.getChef())) {
					return false;
				}
		}

		return true;
	}

	private Integer getTotalPrice(List<Dish> dishes) {
		int totalPrice = 0;
		String previousDishName = "";
		for (Dish dish : dishes) {
			if (!dish.getName().equals(previousDishName)) {
				totalPrice = totalPrice + dish.getPrice();
			}
			previousDishName = dish.getName();
		}

		return totalPrice;
	}

	private boolean isMenuListValid(List<Dish> menu) {
		if (menu.size() < 2) {
			return true;
		}

		Dish previousDish = menu.get(0);
		int allowedDays = previousDish.getDays();
		int totalPrice = previousDish.getPrice();
		for (int i = 1; i < menu.size(); i++) {
			Dish dish = menu.get(i);

			if (!previousDish.getCarbs().contains(ANY) && dish.getCarbs().equals(previousDish.getCarbs()) && !dish.getName().equals(previousDish.getName())) {
				return false;
			}

			if (dish.getName().equals(previousDish.getName())) {
				if (allowedDays == 0) {
					return false;
				}

				allowedDays = allowedDays - 1;
			} else {
				allowedDays = dish.getDays();
				totalPrice = totalPrice + dish.getPrice();
			}

			previousDish = dish;
		}

		if (totalPrice > 15) {
			return false;
		}
		return true;
	}

	public List<Dish> regenerateSingleDishForMenu(Integer dishToChangeIndex, List<Dish> menu) {
		List<Dish> availableDishes = getAllDishes();
		Dish dishToBeReplaced = menu.get(dishToChangeIndex);
		LOGGER.info("Trying to replace dish {} from current menu", dishToBeReplaced.getName());

		availableDishes.removeAll(menu);

		Collections.shuffle(availableDishes);
		Dish dish = availableDishes.remove(0);

		menu.set(dishToChangeIndex, dish);
//		boolean menuIsValid = isMenuListValid(menu);
		boolean menuIsValid = true;
		while (!menuIsValid && !availableDishes.isEmpty()) {
			dish = availableDishes.remove(0);
			menu.set(dishToChangeIndex, dish);

			menuIsValid = isMenuListValid(menu);
		}

		if (!menuIsValid) {
			LOGGER.warn("No new configuration could be generated by replacing the dish at index {} from menu {}", dishToChangeIndex, menu);
			menu.set(dishToChangeIndex, dishToBeReplaced);
		}

		return menu;
	}

	public void saveMenu(List<Dish> dishes) {
		if (dishes.size() == NUMBER_OF_DAYS_IN_WEEK && !dishes.equals(latestSavedMenu)) {
			LOGGER.info("Saving a new menu configuration to file");
			dishRepository.saveMenu(dishes);
			latestSavedMenu = dishes;
		}
	}
}
