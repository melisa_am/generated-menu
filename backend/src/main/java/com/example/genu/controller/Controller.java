package com.example.genu.controller;

import com.example.genu.domain.BalanceType;
import com.example.genu.domain.Dish;

import java.util.List;

public interface Controller {
    List<Dish> getAllDishes();

    List<Dish> generateWeeklyMenu(Integer totalPrice, BalanceType balanceType, boolean allowTakeout);

    List<Dish> getLatestMenu();

    List<Dish> regenerateSingleDishForMenu(Integer dish, List<Dish> menu);

    void saveMenu(List<Dish> menu);
}
