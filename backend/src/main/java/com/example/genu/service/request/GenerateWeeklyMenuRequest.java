package com.example.genu.service.request;

import com.example.genu.domain.BalanceType;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class GenerateWeeklyMenuRequest {
    private Boolean allowTakeout;
    private Integer totalPrice;
    private BalanceType balanceType;
}
