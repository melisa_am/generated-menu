package com.example.genu.service.reply;

import com.example.genu.domain.Status;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Reply {
    private Status status;
}
