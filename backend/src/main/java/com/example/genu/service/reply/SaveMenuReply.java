package com.example.genu.service.reply;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
@Getter
@Setter
public class SaveMenuReply extends Reply {
}
