package com.example.genu.service.request;

import com.example.genu.domain.Dish;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class RegenerateSingleDishForMenuRequest {
    private Integer dishToChangeIndex;
    private List<Dish> menu;
}
