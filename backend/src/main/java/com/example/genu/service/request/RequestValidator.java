package com.example.genu.service.request;

import com.example.genu.toolkit.Validator;

public class RequestValidator {

    public static final int NUMBER_OF_DAYS_PER_MENU = 7;

    public static Validator validateGenerateWeeklyMenuRequest(GenerateWeeklyMenuRequest request) {
        Validator validator = new Validator();
        if (request.getTotalPrice() == null || request.getTotalPrice() == 0) {
            validator.addError("Total menu price cannot be empty");
        }

        if (request.getTotalPrice() < 7 || request.getTotalPrice() > 35) {
            validator.addError("Total menu price cannot be lower than 7 or higher than 35");
        }

        if (request.getBalanceType() == null) {
            validator.addError("Value to equilibrate menu on cannot be null");
        } else {
            switch (request.getBalanceType()) {
                case CARBS:
                case CHEF:
                case VEGETABLE:
                case MEAT:
                    break;
                default:
                    validator.addError("Menu can be equilibrated only on: carbs, proteins, vegetables or chef");
            }
        }

        return validator;
    }

    public static Validator validateRegeneteSingleDishForMenuRequest(RegenerateSingleDishForMenuRequest request) {
        Validator validator = new Validator();
        if (request.getMenu() == null || request.getMenu().size() == 0) {
            validator.addError("Requested menu cannot be empty");
        }

        if (request.getDishToChangeIndex() < 0 || request.getDishToChangeIndex() > NUMBER_OF_DAYS_PER_MENU) {
            validator.addError("Dish index to regenerate cannot be lower than 0 or higher than " + NUMBER_OF_DAYS_PER_MENU);
        }

        return validator;
    }
}
