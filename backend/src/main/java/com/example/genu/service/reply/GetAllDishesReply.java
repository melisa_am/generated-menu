package com.example.genu.service.reply;

import com.example.genu.domain.Dish;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class GetAllDishesReply extends Reply {
    private List<Dish> dishes;
}
