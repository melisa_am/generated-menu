package com.example.genu.service.reply;

import com.example.genu.domain.Dish;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@EqualsAndHashCode
@Getter
@Setter
public class LoadMenuReply extends Reply {
    private List<Dish> menu;
}
