package com.example.genu.service;

import com.example.genu.controller.Controller;
import com.example.genu.controller.MenuGeneratorController;
import com.example.genu.domain.Dish;
import com.example.genu.domain.Status;
import com.example.genu.service.reply.*;
import com.example.genu.service.request.GenerateWeeklyMenuRequest;
import com.example.genu.service.request.RegenerateSingleDishForMenuRequest;
import com.example.genu.service.request.RequestValidator;
import com.example.genu.service.request.SaveMenuRequest;
import com.example.genu.toolkit.Validator;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RequestMapping("/api")
@RestController
public class MenuGeneratorService {
    private Controller controller = new MenuGeneratorController("backend/src/main/resources/dishes.json");

    @ResponseBody
    @RequestMapping(value = "/getAllDishes", method = RequestMethod.GET)
    public GetAllDishesReply getAllDishes() {
        GetAllDishesReply getAllDishesReply = new GetAllDishesReply();
        try {
            List<Dish> dishes = controller.getAllDishes();
            getAllDishesReply.setDishes(dishes);
            getAllDishesReply.setStatus(new Status(Status.Level.OK));
        } catch (Exception e) {
            getAllDishesReply.setDishes(Collections.emptyList());
            getAllDishesReply.setStatus(new Status(Status.Level.ERROR, e.getMessage()));
        }
        return getAllDishesReply;
    }

    @PostMapping(value = "/generateWeeklyMenu")
    @ResponseBody
    public GenerateWeeklyMenuReply getWeeklyMenu(@RequestBody GenerateWeeklyMenuRequest generateWeeklyMenuRequest) {
        GenerateWeeklyMenuReply generateWeeklyMenuReply = new GenerateWeeklyMenuReply();
        Validator validator = RequestValidator.validateGenerateWeeklyMenuRequest(generateWeeklyMenuRequest);
        if (!validator.isValid()) {
            generateWeeklyMenuReply.setMenu(new ArrayList<>());
            generateWeeklyMenuReply.setStatus(new Status(Status.Level.ERROR, validator.getFormatedErrors()));
        } else {
            try {
                List<Dish> menu = controller.generateWeeklyMenu(
                    generateWeeklyMenuRequest.getTotalPrice(),
                    generateWeeklyMenuRequest.getBalanceType(),
                    generateWeeklyMenuRequest.getAllowTakeout()
                );
                generateWeeklyMenuReply.setMenu(menu);
                generateWeeklyMenuReply.setStatus(new Status(Status.Level.OK));
            } catch (Exception e) {
                generateWeeklyMenuReply.setMenu(Collections.emptyList());
                generateWeeklyMenuReply.setStatus(new Status(Status.Level.ERROR, e.getMessage()));
            }
        }

        return generateWeeklyMenuReply;
    }

    @ResponseBody
    @RequestMapping(value = "/loadMenu", method = RequestMethod.GET)
    public LoadMenuReply loadMenu() {
        LoadMenuReply loadMenuReply = new LoadMenuReply();
        try {
            List<Dish> dishes = controller.getLatestMenu();
            loadMenuReply.setMenu(dishes);
            loadMenuReply.setStatus(new Status(Status.Level.OK));
        } catch (Exception e) {
            loadMenuReply.setMenu(Collections.EMPTY_LIST);
            loadMenuReply.setStatus(new Status(Status.Level.ERROR, e.getMessage()));
        }
        return loadMenuReply;
    }

    @PostMapping(value = "/regenerateSingleDish")
    @ResponseBody
    public RegenerateSingleDishForMenuReply regenerateSingleDishForMenu(@RequestBody RegenerateSingleDishForMenuRequest regenerateSingleDishForMenuRequest) {
        RegenerateSingleDishForMenuReply regenerateSingleDishForMenuReply = new RegenerateSingleDishForMenuReply();
        Validator validator = RequestValidator.validateRegeneteSingleDishForMenuRequest(regenerateSingleDishForMenuRequest);
        if (!validator.isValid()) {
            regenerateSingleDishForMenuReply.setMenu(Collections.EMPTY_LIST);
            regenerateSingleDishForMenuReply.setStatus(new Status(Status.Level.ERROR, validator.getFormatedErrors()));
        } else {
            try {
                List<Dish> menu = controller.regenerateSingleDishForMenu(
                    regenerateSingleDishForMenuRequest.getDishToChangeIndex(),
                    regenerateSingleDishForMenuRequest.getMenu()
                );
                regenerateSingleDishForMenuReply.setMenu(menu);
                regenerateSingleDishForMenuReply.setStatus(new Status(Status.Level.OK));
            } catch (Exception e) {
                regenerateSingleDishForMenuReply.setMenu(Collections.emptyList());
                regenerateSingleDishForMenuReply.setStatus(new Status(Status.Level.ERROR, e.getMessage()));
            }
        }

        return regenerateSingleDishForMenuReply;
    }

    @PostMapping(value = "/saveMenu")
    @ResponseBody
    public SaveMenuReply saveMenu(@RequestBody SaveMenuRequest saveMenuRequest) {
        SaveMenuReply saveMenuReply = new SaveMenuReply();
        try {
            controller.saveMenu(saveMenuRequest.getMenu());
            saveMenuReply
                    .setStatus(new Status(Status.Level.OK));
        } catch (Exception e) {
            saveMenuReply
                    .setStatus(new Status(Status.Level.ERROR, e.getMessage()));
        }

        return saveMenuReply;
    }
}

