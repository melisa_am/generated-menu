package com.example.genu.service.request;

import com.example.genu.domain.Dish;
import lombok.Getter;

import java.util.List;

@Getter
public class SaveMenuRequest {
    private List<Dish> menu;

    public SaveMenuRequest() {
    }
}
