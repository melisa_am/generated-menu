package com.example.genu.domain;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@RequiredArgsConstructor
@AllArgsConstructor
public class Status {
    public enum Level {
        OK,
        ERROR
    }

    public final Level flag;
    public String message = "";
}


