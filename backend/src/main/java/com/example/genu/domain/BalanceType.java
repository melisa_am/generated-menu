package com.example.genu.domain;

public enum BalanceType {
    CARBS,
    MEAT,
    VEGETABLE,
    CHEF
}
