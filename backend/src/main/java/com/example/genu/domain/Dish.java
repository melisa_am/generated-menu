package com.example.genu.domain;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
public class Dish {
	private String name;
	private List<String> carbs;
	private List<String> meat;
	private List<String> vegetable;
	private List<String> chef;
	private Integer price;
	private Integer days;

	public static class DishBuilder {
		private String name;
		private List<String> carbs;
		private List<String> meat;
		private List<String> vegetable;
		private List<String> chef;
		private Integer price;
		private Integer days;

	public static DishBuilder newBuilder() {
			return new DishBuilder();
		}

		public Dish build() {
			return new Dish(
				name,
				carbs,
				meat,
				vegetable,
				chef,
				price,
				days
			);
		}

		public DishBuilder setName(String name) {
			this.name = name;
			return this;
		}

		public DishBuilder setCarbs(List<String> carbs) {
			this.carbs = carbs;
			return this;
		}

		public DishBuilder setMeat(List<String> meat) {
			this.meat = meat;
			return this;
		}

		public DishBuilder setVegetable(List<String> vegetable) {
			this.vegetable = vegetable;
			return this;
		}

		public DishBuilder setChef(List<String> chef) {
			this.chef = chef;
			return this;
		}

		public DishBuilder setPrice(Integer price) {
			this.price = price;
			return this;
		}

		public DishBuilder setDays(Integer days) {
			this.days = days;
			return this;
		}
	}
}
