package com.example.genu.repository;

import com.example.genu.domain.Dish;

import java.util.List;

public interface DishRepository {
    List<Dish> getDishes();
    void saveMenu(List<Dish> dishes);
    List<Dish> getMenuIfExists();

    boolean isCurrentDateLoaded();
}
