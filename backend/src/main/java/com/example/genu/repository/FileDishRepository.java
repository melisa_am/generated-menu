package com.example.genu.repository;

import com.example.genu.domain.Dish;
import com.example.genu.toolkit.JsonToolkit;
import com.example.genu.toolkit.Validator;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;

public class FileDishRepository implements DishRepository {
	private final String GENERATED_MENU_PATH = "backend/src/main/resources/";
	private final String GENERATED_MENU_TXT_TYPE = ".json";
	private final String GENERATED_MENU_FILE_NAME_SUFFIX = "MenuDishes - ";
	private final Logger LOGGER = LogManager.getLogger(FileDishRepository.class);
	private final String fileName;

	private String lastLoadedDate;

	@Getter
	private final List<Dish> dishes;

	public FileDishRepository(String fileName) {
		this.fileName = fileName;
		dishes = getAllDishesFromFile();
	}

	public List<Dish> getAllDishesFromFile() {
		return getDishesFromFile(fileName);
	}

	public List<Dish> getDishesFromFile(String fileName) {
		JSONParser jsonParser = new JSONParser();
		List<Dish> newDishes = new ArrayList<>();

		Validator validator = new Validator();
		try (FileReader reader = new FileReader(fileName)) {
			Object obj = jsonParser.parse(reader);
			JSONArray objectList = (JSONArray) obj;

			objectList.forEach(object -> {
				newDishes.add(JsonToolkit.parseDishObject((JSONObject) object, validator));
			});

			if (!validator.isValid()) {
				LOGGER.error(
						"Could not read all dishes from file. Errors: {}}",
						() -> String.join(",\n ", validator.getErrors())
				);
			}
		} catch (IOException | ParseException e) {
			LOGGER.error("Found the following errors while trying to get dishes from file {}", e::getMessage);
		}

		return newDishes;
	}

	public List<Dish> getMenuIfExists() {
		String currentWeekMonday = getMondayDateForCurrentWeek();
		List<Dish> dishes = getDishesFromFile(GENERATED_MENU_PATH + getFileName(currentWeekMonday));

		if (dishes != null && !dishes.isEmpty()) {
			lastLoadedDate = currentWeekMonday;
		}

		LOGGER.info("Found {} dishes generated for the current week {}", dishes.size(), currentWeekMonday);

		return dishes;
	}

	public void saveMenu(List<Dish> dishes) {
		String fileName = getFileName(getMondayDateForCurrentWeek());
		writeDishesToFile(dishes, fileName);
	}

	public boolean isCurrentDateLoaded() {
		if (lastLoadedDate == null) {
			return false;
		}

		if (!lastLoadedDate.equals(getMondayDateForCurrentWeek())) {
			return false;
		}

		return true;
	}

	private void writeDishesToFile(List<Dish> dishes, String fileName) {
		final FileWriter writer;
		List<String> dishesAsJsonString = new ArrayList<>();

		LOGGER.info("Writing {} dishes to the file {}", () -> dishes.size(), () -> fileName);
		try {
			writer = new FileWriter(GENERATED_MENU_PATH + fileName);
			writer.write("[\n");
			dishes.forEach(
				dish -> {
					dishesAsJsonString.add(JsonToolkit.translateDishToJson(dish));
				}
			);

			writer.write(String.join(",\n ", dishesAsJsonString));
			writer.write("\n]");
			writer.close();
		} catch (IOException e) {
			LOGGER.error("Found the following errors while trying to write dishes to file {}", e::getMessage);
		}
	}

	private String getFileName(String date) {
		return GENERATED_MENU_FILE_NAME_SUFFIX + date + GENERATED_MENU_TXT_TYPE;
	}

	private String getMondayDateForCurrentWeek() {
		Instant instant = Instant.now();
		ZoneId zoneId = ZoneId.of("GMT");
		ZonedDateTime curentZonedDateTime = ZonedDateTime.ofInstant(instant, zoneId);
		ZonedDateTime  mondayDateForCurrentWeek = curentZonedDateTime.with(ChronoField.DAY_OF_WEEK, 1);

		String mondayDateAsString = mondayDateForCurrentWeek.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

		LOGGER.info("The corresponding Monday date for this week is {}", () -> mondayDateAsString);

		return mondayDateAsString;
	}
}
