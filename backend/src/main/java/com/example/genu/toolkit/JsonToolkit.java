package com.example.genu.toolkit;

import com.example.genu.domain.Dish;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.util.Strings;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonToolkit {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String translateDishToJson(Dish dish) {
        try {
            String dishJson = objectMapper.writeValueAsString(dish);
            return "{\n\"dish\":" + dishJson + "\n}";
        }
        catch (IOException e) {
            throw new RuntimeException(String.format("Could not generate the json from Dish. Exception: %s", e.getMessage()));
        }
    }

    public static Dish parseDishObject(JSONObject dishObject, Validator validator) {
        JSONObject dish = (JSONObject) dishObject.get("dish");
        Dish.DishBuilder dishBuilder = Dish.DishBuilder.newBuilder();

        String name = (String) dish.get("name");
        if (name == null || Strings.EMPTY.equals(name)) {
            validator.addError("Missing name from dish object");
        }
        dishBuilder.setName(name);

        List carbs = (ArrayList) dish.get("carbs");
        if (carbs == null) {
            validator.addError("Missing carbs from dish with name %s", name);
        } else {
            dishBuilder.setCarbs(carbs);
        }

        List meat = (ArrayList) dish.get("meat");
        if (meat == null) {
            validator.addError("Missing meat from dish with name %s", name);
        } else {
            dishBuilder.setMeat(meat);
        }

        List vegetable = (ArrayList) dish.get("vegetable");
        if (vegetable == null) {
            validator.addError("Missing vegetable from dish with name %s", name);
        } else {
            dishBuilder.setVegetable(vegetable);
        }

        List chef = (ArrayList) dish.get("chef");
        if (chef == null) {
            validator.addError("Missing chef from dish with name %s", name);
        } else {
            dishBuilder.setChef(chef);
        }

        int price;
        try {
            price = ((Long) dish.get("price")).intValue();
            dishBuilder.setPrice(price);
        } catch (Exception e) {
            validator.addError("Could not find price of type long for dish %s", name);
        }

        int days;
        try {
            days = ((Long) dish.get("days")).intValue();
            dishBuilder.setDays(days);
        } catch (Exception e) {
            validator.addError("Could not find days of type long for dish %s", name);
        }

        return dishBuilder.build();
    }
}
