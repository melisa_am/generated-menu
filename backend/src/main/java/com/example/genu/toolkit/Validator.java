package com.example.genu.toolkit;

import org.apache.tomcat.util.buf.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class Validator {
    List<String> errors;

    public Validator() {
        errors = new ArrayList<>();
    }

    public List<String> getErrors() {
        return errors;
    }

    public boolean isValid() {
        return errors.isEmpty();
    }

    public void addError(String error) {
        errors.add(error);
    }

    public void addError(String error, String... parameters) {
        errors.add(String.format(error, (Object[]) parameters));
    }

    public String getFormatedErrors() {
        return StringUtils.join(errors, ',');
    }
}
