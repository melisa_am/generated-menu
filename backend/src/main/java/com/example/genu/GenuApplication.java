package com.example.genu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenuApplication {
	public static void main(String[] args) {
		SpringApplication.run(GenuApplication.class, args);
	}
}
