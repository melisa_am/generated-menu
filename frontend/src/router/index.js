import Vue from "vue";
import VueRouter from "vue-router";
import AllDishes from "../components/AllDishes.vue";
import MainPage from "../components/MainPage.vue";
import Menu from "../components/Menu.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Main Page",
    component: MainPage,
    meta: {
      title: 'Genu'
    }
  },
  {
    path: "/all-dishes",
    name: "All Dishes",
    component: AllDishes,
    meta: {
      title: 'Genu - All dishes'
    }
  },
  {
    path: "/menu",
    name: "Menu",
    component: Menu,
    meta: {
      title: 'Genu - Menu'
    }
  }
];

const router = new VueRouter({
  routes
});

export default router;
