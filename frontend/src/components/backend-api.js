import axios from "axios";

const AXIOS = axios.create({
  baseURL: `/api`,
  timeout: 1000
});

export default {
  // getAllDishes() {
  //   axios
  //       .get(`api/getAllDishes`)
  //       .then(response => {
  //         console.log(response.data);
  //         this.items = response.data;
  //       })
  //       .catch(e => {
  //         this.errors.push(e);
  //       })
  // },
  // saveMenu() {
  //   axios
  //       .post("api/saveWeeklyMenu", {
  //         menu: this.items
  //       })
  //       .then(response => {
  //         console.log("Saved menu");
  //         console.log(response);
  //       })
  //       .catch(e => {
  //         this.errors.push(e);
  //       })
  // },
  // generateMenu() {
  //   axios
  //       .get("api/generateWeeklyMenu")
  //       .then(response => {
  //         console.log(response.data);
  //         this.items = response.data;
  //       })
  //       .catch(e => {
  //         this.errors.push(e);
  //       })
  // },
  // getMenu() {
  //   axios
  //       .get("api/loadMenu")
  //       .then(response => {
  //         console.log(response.data);
  //         this.items = response.data;
  //       })
  //       .catch(e => {
  //         this.errors.push(e);
  //       });
  // },
  // regenerate(index) {
  //   axios
  //       .post("api/regenerateSingleDish", {
  //         dishToChangeIndex: index,
  //         menu: this.items
  //       })
  //       .then(response => {
  //         console.log("Going to refresh menu table");
  //         this.items = response.data;
  //         this.refreshTable();
  //         console.log(response);
  //       })
  //       .catch(e => {
  //         this.errors.push(e);
  //       });
  // }
};
